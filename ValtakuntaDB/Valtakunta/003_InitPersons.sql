use DnDValtakunta;
go
set identity_insert person on;
go
delete from armyUnit;
delete from guildTradeRoute;
delete from tradeRoute;
delete from holdings;
delete from province;
delete from domain;
delete from person;

insert into person (id,name,description) values 
	(1,'Herne III','Hernemaan Kuningas.'),
	(2,'Profeetta','Kulttijohtaja.'),
	(3,'Frederick Festungdorf','Hernari provinssissa asuva aatelismies. Käyttänyt joskus salanimeä Jaska'),
	(4,'Velho','Barbaarian provinssissa asuva necromancer.'),
	(5,'Expexter','Soturi ja seikkailija.'),
	(6,'Matcher','Iin taikuri ja seikkailija. Omistaa taikashown Hernarin kaupungissa.'),
	(7,'Soncon','Barbaari ja seikkailija Barbaarian provinssista.'),
	(8,'Kaarle II','Akvilonian Hallitsija'),
	(9,'Rotanhäntä','Rottakoplan johtaja');
go
set identity_insert person off;



go
/*
insert into domain (id,name, regent ) values 
(1,'Hernemaa',1),
(2,'Vihermaa',8),
(3,'Uuden Ajan Valtakunta',2);
go*/


-- set identity_insert province on;



-- Hernemaa(#1)
insert into domain (name, regent, courtLvl  ) values 
('Hernemaa',1, 4 );

--EXEC createProvince  @domainName = '',@provinceName = '', @terrainName = '',	@coastValue = 0, @level = , @loyalityValue = 2, @taxRate = 2;
EXEC createProvince  @domainName = 'Hernemaa',@provinceName = 'Hernari', @terrainName = 'Light Forest',	@coastValue = 1, @level = 4,@fortLevel = 4, @loyalityValue = 2, @taxRate = 2;
EXEC addHighway @provinceName = 'Hernari'; EXEC addSeaport @provinceName = 'Hernari'; 
EXEC setBridges @provinceName = 'Hernari', @woodBridges = 0, @stoneBridges = 0, @ferries = 1;
EXEC createProvince  @domainName = 'Hernemaa',@provinceName = 'Metsäkylä', @terrainName = 'Heavy Forest',	@coastValue = 1, @level = 2,@fortLevel = 1, @loyalityValue = 2, @taxRate = 2;
EXEC addHighway @provinceName = 'Metsäkylä';
--EXEC addHighway @provinceName = 'Metsäkylä';
EXEC createProvince  @domainName = 'Hernemaa',@provinceName = 'Kaivoksela', @terrainName = 'Low Mountains',	@coastValue = 0, @level = 2,@fortLevel = 2, @loyalityValue = 2, @taxRate = 2;
EXEC addHighway @provinceName = 'Kaivoksela';
--EXEC addHighway @provinceName = 'Kaivoksela';

EXEC createHolding @holdingsName = 'Hernari_kuninkaankauppa',@holdingsType = 'Guild', @domainName = 'Hernemaa', @locationName = 'Hernari', @holdingsLvl = 1;
EXEC holdings_setFortLvl @holdingsName = 'Hernari_kuninkaankauppa', @fortLvl = 1;
EXEC createHolding @holdingsName = 'Hernari_poliisi',@holdingsType = 'Law', @domainName = 'Hernemaa', @locationName = 'Hernari', @holdingsLvl = 2;
EXEC holdings_setFortLvl @holdingsName = 'Hernari_poliisi', @fortLvl = 2;
EXEC createHolding @holdingsName = 'Hernarin Hernekirkko',@holdingsType = 'Temple', @domainName = 'Hernemaa', @locationName = 'Hernari', @holdingsLvl = 4;
EXEC holdings_setFortLvl @holdingsName = 'Hernarin Hernekirkko', @fortLvl = 1;

EXEC createHolding @holdingsName = 'Metsäkylän Hernekirkko',@holdingsType = 'Temple', @domainName = 'Hernemaa', @locationName = 'Metsäkylä', @holdingsLvl = 1;
EXEC createHolding @holdingsName = 'Hernari Metsäkylän metsurit',@holdingsType = 'Guild', @domainName = 'Hernemaa', @locationName = 'Metsäkylä', @holdingsLvl = 1;
EXEC createHolding @holdingsName = 'Hernari Metsäkylän poliisi',@holdingsType = 'Law', @domainName = 'Hernemaa', @locationName = 'Metsäkylä', @holdingsLvl = 1;

EXEC createHolding @holdingsName = 'Kaivokselan Hernekirkko',@holdingsType = 'Temple', @domainName = 'Hernemaa', @locationName = 'Kaivoksela', @holdingsLvl = 2;
EXEC createHolding @holdingsName = 'Hernari kaivokselan kaivosyhtiö',@holdingsType = 'Guild', @domainName = 'Hernemaa', @locationName = 'Kaivoksela', @holdingsLvl = 1;
EXEC createHolding @holdingsName = 'Hernari kaivokselan poliisi',@holdingsType = 'Law', @domainName = 'Hernemaa', @locationName = 'Kaivoksela', @holdingsLvl = 1;

-- Akvilonia (#2)
insert into domain (name, regent, courtLvl ) values 
	('Akvilonia',8,6);
--- Akvilonian pääkaupunkiprovinssi Akvilon
EXEC createProvince  @domainName = 'Akvilonia',@provinceName = 'Akvilon', @terrainName = 'Plains',	@coastValue = 1, @level = 9,@fortLevel = 6, @loyalityValue = 2, @taxRate = 2;
EXEC addHighway @provinceName = 'Akvilon'; EXEC addSeaport @provinceName = 'Akvilon';

EXEC createHolding @holdingsName = 'Akvilon_kuninkaankauppa',@holdingsType = 'Guild', @domainName = 'Akvilonia', @locationName = 'Akvilon', @holdingsLvl = 6;
EXEC createHolding @holdingsName = 'Akvilon_poliisi',@holdingsType = 'Law', @domainName = 'Akvilonia', @locationName = 'Akvilon', @holdingsLvl = 6;
EXEC holdings_setFortLvl @holdingsName = 'Akvilon_poliisi', @fortLvl = 6;
EXEC createHolding @holdingsName = 'Akvilon_kirkko',@holdingsType = 'Temple', @domainName = 'Akvilonia', @locationName = 'Akvilon', @holdingsLvl = 2;

-- UudenAjanValtakunta(#3)
insert into domain (name, regent, courtLvl  ) values 
 ('Uuden Ajan Valtakunta',2, 0);
EXEC createHolding @holdingsName = 'Profeetan temppeli',@holdingsType = 'Temple', @domainName = 'Uuden Ajan Valtakunta', @locationName = 'Metsäkylä', @holdingsLvl = 1;
EXEC holdings_setFortLvl @holdingsName = 'Profeetan temppeli', @fortLvl = 1;

--- Festungdorfin liikemiesimperiumi
insert into domain (name, regent, courtLvl  ) values
	('Festung yhtiöt',3,0);

EXEC createHolding @holdingsName = 'Hernari_Festungmarket',@holdingsType = 'Guild', @domainName = 'Festung yhtiöt', @locationName = 'Hernari', @holdingsLvl = 2;
EXEC createHolding @holdingsName = 'Hernari_FestungTurvatiimi',@holdingsType = 'Law', @domainName = 'Festung yhtiöt', @locationName = 'Hernari', @holdingsLvl = 2;
EXEC holdings_setFortLvl @holdingsName = 'Hernari_FestungTurvatiimi', @fortLvl = 2;

--- Rotanhännän mafia
insert into domain (name, regent, courtLvl  ) values
 ('Rottayhtiöt',9,0);

EXEC createHolding @holdingsName = 'Hernari_Rottayhtiön pääkonttori',@holdingsType = 'Guild', @domainName = 'Rottayhtiöt', @locationName = 'Hernari', @holdingsLvl = 1;
EXEC holdings_setFortLvl @holdingsName = 'Hernari_Rottayhtiön pääkonttori', @fortLvl = 1;

--- Luodaan kauppareittejä, ne voidaan luoda vasta nyt kun provinssit on luotu.
--- Hernariin voidaan luoda 1 kauppareitti joka kulkee hernarin ja akvilonin välillä

EXEC createTradeRoute @guildA='Akvilon_kuninkaankauppa',@guildB='Hernari_kuninkaankauppa',@routeName='searoute_AkvilonHernari_1';
--- metsäkylän puureitti rottakoplan kiltaan.
EXEC createTradeRoute @guildA='Hernari Metsäkylän metsurit',@guildB='Hernari_Rottayhtiön pääkonttori',@routeName='landroute_Metsäkylä-Hernari-puukauppa';
--- kaivokselan mineraalireitti festung yhtiöiden kiltaan.
EXEC createTradeRoute @guildA='Hernari kaivokselan kaivosyhtiö',@guildB='Hernari_Festungmarket',@routeName='landroute_Kaivoksela-Hernari-kaivostuotteet';

go


--- luodaan armeijat... 
--- Hernarille
EXEC createArmyUnit @unitName='Kaartin keihäsmiehet', @ownerDomain='Hernemaa',@unitType='Heavy Footmen',@locationProvince='Hernari';
EXEC createArmyUnit @unitName='Kaartin varsijousimiehet', @ownerDomain='Hernemaa',@unitType='Heavy Footmen',@locationProvince='Hernari';
EXEC createArmyUnit @unitName='Kaartin ratsumiehet', @ownerDomain='Hernemaa',@unitType='Medium Cavalry',@locationProvince='Hernari';

EXEC createArmyUnit @unitName='Metsäkylän kaarti', @ownerDomain='Hernemaa',@unitType='Heavy Footmen',@locationProvince='Metsäkylä';
EXEC createArmyUnit @unitName='Kaivokselan kaarti', @ownerDomain='Hernemaa',@unitType='Heavy Footmen',@locationProvince='Kaivoksela';
--- luodaan laivasto...
--- Hernarille

