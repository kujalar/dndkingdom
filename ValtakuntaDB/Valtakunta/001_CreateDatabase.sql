﻿use DnDValtakunta;
go
/* 
https://www.mssqltips.com/sqlservertip/1543/using-sqlcmd-to-execute-multiple-sql-server-scripts/
*/
-- drop procedures
DROP PROCEDURE createArmyUnit;
DROP PROCEDURE holdings_setFortLvl;
DROP PROCEDURE setBridges;
DROP PROCEDURE createTradeRoute;
DROP PROCEDURE createProvince;
DROP PROCEDURE createHolding;
DROP PROCEDURE addSeaport;
DROP PROCEDURE addHighway;

DROP FUNCTION provinceMaintenanceCostFunc;
-- drop views
DROP VIEW armyDomainSumView;
DROP VIEW armyUnitView;
DROP VIEW domainSumView;
--DROP VIEW domainCountView;
DROP VIEW maakuntaDomainSumView;
DROP VIEW holdingDomainCollectionsSumView;
DROP VIEW holdingsTradeRouteSums;
DROP VIEW holdingsView;
DROP VIEW maakunnatView;
-- drop tables
drop table armyUnit;
go
drop table landUnitType;
go
drop table guildTradeRoute;
go
drop table tradeRoute;
go
drop table holdings;
go
drop table province;
go
drop table domain;
go
drop table loyalty;
go
drop table taxes;
go
drop table terrain;
go
drop table person;
go
drop table holdingsType;
go
--drop table provinceTaxation;
--go
drop table provinceLvl;
go
--drop table guildAndTempleCollection;
--go

create table provinceLvl (
	id integer,
	description text,
	population integer,
	primary key(id)
	 
);
go

create table person (
	id integer identity,
	name varchar(255),
	description text,
	primary key (id),
	CONSTRAINT person_name UNIQUE(name)
);
go

create table holdingsType (
	id integer identity,
	name nvarchar(16),
	generateCollection integer,
	incomeLvl integer, --- how many thirds one lvl generates gold 3 => 3/3 = 1 GB / lvl 
	primary key (id)
);
go

create table terrain (
	id integer identity,
	name varchar(24),
	maxProvinceLvl integer,
	basePotential integer,
	moveCost integer,
	primary key (id),
);
go

create table loyalty (
	id integer,
	description varchar(16),
	primary key(id)
);
go
create table taxes (
	id integer,
	description varchar(16),
	primary key(id)
);
create table domain (
	id integer identity,
	regent integer,
	name varchar(255),
	courtLvl integer,
	primary key (id),
	CONSTRAINT fk_domain_person FOREIGN KEY (regent) REFERENCES person(id)
);
go

create table province (
    id integer identity,
	domain integer,
	loyalty integer,
	terrain integer,
	woodBridges integer,
	stoneBridges integer,
	ferries integer,
	coast integer,
	seaport integer,
	highway integer,
	name varchar(255) unique,
	lvl integer,
	fortLvl integer,
	maxLvl integer,
	maxSource integer,
	taxrate integer,
	primary key (id),
	CONSTRAINT fk_province_provinceLvl FOREIGN KEY (lvl) REFERENCES provinceLvl(id),
	CONSTRAINT fk_province_domain FOREIGN KEY (domain) REFERENCES domain(id),
	CONSTRAINT fk_province_terrain FOREIGN KEY (terrain) REFERENCES terrain(id),
	CONSTRAINT fk_province_loyalty FOREIGN KEY (loyalty) REFERENCES loyalty(id),
	CONSTRAINT fk_province_taxrate FOREIGN KEY (taxrate) REFERENCES taxes(id)
);
go

create table holdings (
	id integer identity,
	name varchar(255) unique,
	type integer,
	location integer,
	ownerDomain integer,
	lvl integer,
	fortLvl integer,
	primary key (id),
	CONSTRAINT fk_holdings_holdingstype FOREIGN KEY (type) REFERENCES holdingsType(id),
	CONSTRAINT fk_holdings_province FOREIGN KEY (location) REFERENCES province(id),
	CONSTRAINT fk_holdings_domain FOREIGN KEY (ownerDomain) REFERENCES domain(id)
);
go
create table landUnitType (
	id integer identity,
	name varchar(255) unique,
	baseMusterCost integer,
	baseMaintenanceCost integer,---aktiivisen osaston hinta, puolittuu jos on garrisonissa (GBn 12th osissa)
	draft integer,
	mercenary integer,
	primary key (id)
);
go
--- Land Forces
create table armyUnit (
	id integer identity,
	name varchar(255) unique,
	size integer,
	type integer,
	race integer,--TODO
	mercenary integer,
	currentLocation integer,
	homeProvince integer,
	ownerDomain integer,
	primary key (id),
	CONSTRAINT fk_armyUnit_currentLocation FOREIGN KEY (currentLocation) REFERENCES province(id),
	CONSTRAINT fk_armyUnit_homeProvince FOREIGN KEY (homeProvince) REFERENCES province(id),
	CONSTRAINT fk_armyUnit_ownerDomain FOREIGN KEY (ownerDomain) REFERENCES domain(id),
	CONSTRAINT fk_armyUnit_type FOREIGN KEY (type) REFERENCES landUnitType(id)
);
go

--- Naval Forces



--- there is tradeRoute with id because 2 guilds may share the same trade route.
create table tradeRoute (
	id integer identity,
	name varchar(255) unique,
	primary key (id)
);
go
--- this table attaches tradeRoute to guild holding
create table guildTradeRoute (
	id integer identity,
	tradeRouteId integer not null,
	guildId integer not null,
	primary key (id),
	CONSTRAINT fk_guildTradeRoute_tradeRoute FOREIGN KEY (tradeRouteId) REFERENCES tradeRoute(id),
	CONSTRAINT fk_guildTradeRoute_guild FOREIGN KEY (guildId) REFERENCES holdings(id)
);
go
/*
create table provinceTaxation (
	id integer identity,
	provinceLvl integer,
	taxRate integer,
	taxation text,
	average float,
	primary key(id)
);
go

create table guildAndTempleCollection (
	id integer identity,
	provinceLvl integer,
	holdingLvl integer,
	collection text,
	average float,
	primary key(id)
);
go*/

/* http://www.birthright.net/forums/showwiki.php?title=BRCS:Chapter_five_Ruling_a_domain_Domain_collections
 EI tarvita vanhan kaltaista, tehdään yksinkertaisempi sääntö, antaa tuloksen GB:n 12. osissa*/
CREATE FUNCTION provinceMaintenanceCostFunc(@woodBridges int, @stoneBridges int, @ferries int, @fortLvl int, @seaport int,@highway int,@moveCost int)
returns int
with execute as caller
AS
BEGIN
	DECLARE @maintenanceCost int;
	-- TODO tähän puuttuvia geneerisiä maksuja
	SET @maintenanceCost = @highway * (@moveCost*2) + @seaport*6 + (3*@woodBridges)+(6*@stoneBridges)+(1*@ferries)+(8*@fortLvl);

	RETURN(@maintenanceCost);
END

go
-- näkymä jolla haetaan trade-route summat holdingille.
CREATE VIEW holdingsTradeRouteSums AS
	select h.id as holdingId,count(gTr.id) as countGuildRoutes
	from holdings h
		left outer join guildTradeRoute gTr ON gTr.guildId = h.id
	group by h.id;
go
-- Nakymä jolla haetaan holdingsit
CREATE VIEW holdingsView AS 
	select h.id as id,h.name as name,ownerDomain.name as haltija,hType.name as type,h.fortLvl as fortLevel,h.lvl as level,locationProvince.name as sijainti,
		--(select ISNULL(gtCol.collection,'-')) as collection, 
		--(select ISNULL(gtCol.average,0)) as avgCollection,
		(hType.incomeLvl*h.lvl)+(tradeRouteSums.countGuildRoutes*h.lvl*6)-(4*h.fortLvl) as incomeInTwelfts,
		(hType.incomeLvl*h.lvl) as baseIncome, (tradeRouteSums.countGuildRoutes*h.lvl*6) as tradeIncome,
		(4*h.fortLvl) as fortMaintenance,
		tradeRouteSums.countGuildRoutes as countRoutes, 
		maxRouteCount = 
		CASE
			WHEN hType.name = 'Guild' AND h.lvl >= 1 AND h.lvl <=3 THEN 1
			WHEN hType.name = 'Guild' AND h.lvl >3 AND h.lvl <=6 THEN 2
			WHEN hType.name = 'Guild' AND h.lvl >= 7 THEN 3
			ELSE 0 
		END,
		ownerDomain.id as ownerId, h.location as location
	from holdings h 
		inner join province locationProvince on locationProvince.id = h.location
		inner join holdingsType hType on hType.id = h.type
		, domain ownerDomain, holdingsTradeRouteSums tradeRouteSums
	where ownerDomain.id = h.ownerDomain and tradeRouteSums.holdingId = h.id;
go

-- TODO maakunnatView:n voisi ehkä lisätä vielä näkyviin domainin johon se kuuluu ja lisäksi lisätietoja maastosta yms. sekä id:t
CREATE VIEW maakunnatView AS 
	select p.id,dom.name as valtakunta, p.name as name,p.fortLvl, p.lvl,pLvl.description as lvlDesc,
		(select ISNULL(sum(guilds.level),0) from holdingsView guilds where guilds.type = 'Guild' and guilds.location = p.id ) as guilds,
		(select ISNULL(sum(law.level),0) from holdingsView law where law.type = 'Law' and law.location = p.id ) as law,
		(select ISNULL(sum(temples.level),0) from holdingsView temples where temples.type = 'Temple' and temples.location = p.id ) as temples,
		(SELECT IIF(p.maxSource > p.lvl,p.maxSource-p.lvl,0)) as sourcePotential,
		(select ISNULL(sum(sources.level),0) from holdingsView sources where sources.type = 'Source' and sources.location = p.id ) as sources,
		ter.name as terrain,(select IIF(p.coast = 0,'no','yes')) as coast,
		pLvl.population as population, p.lvl as incomeGB,
		dbo.provinceMaintenanceCostFunc(p.woodBridges,p.stoneBridges,p.ferries,p.fortLvl,p.seaport,p.highway,ter.moveCost) as maintenanceInTwelfts,
		l.description as loyalty,
		--tax.description as taxrate,
		--pTaxation.taxation as taxation,pTaxation.average as average, 
		dom.id as domainId,l.id as loyaltyId, tax.id as taxId

	from province p, loyalty l, terrain ter, taxes tax, domain dom, provinceLvl pLvl
	where l.id = p.loyalty and ter.id = p.terrain and tax.id = p.taxrate and dom.id = p.domain and pLvl.id = p.lvl; 
go






CREATE VIEW maakuntaDomainSumView AS
	select d.id as domainId,ISNULL(sum(maakunnat.lvl),0) as sumProvinceLvl, ISNULL(sum(maakunnat.population),0) as population, 
	ISNULL(sum(maakunnat.incomeGB),0) as incomeGB,
	ISNULL(sum(maakunnat.maintenanceInTwelfts),0) as maintenanceInTwelfts,
	count(maakunnat.id) as lkmProvinces 
	-- ISNULL(sum(maakunnat.average),0) as sumProvinceTaxationAverage
	from domain d left outer join maakunnatView maakunnat on maakunnat.domainId = d.id
	group by d.id;
go	 
CREATE VIEW holdingDomainCollectionsSumView AS
	select d.id as domainId, count(holdings.id) as lkmHoldings,ISNULL(sum(holdings.level),0) as sumHoldingsLvl,
	ISNULL(sum(holdings.incomeInTwelfts),0) as incomeInTwelfts
	--ISNULL(sum(holdings.avgCollection),0) as sumAvgCollection
	from domain d left outer join holdingsView holdings on holdings.ownerId = d.id
	group by d.id;
go


CREATE VIEW armyUnitView AS
	select unit.*,unitType.name as unitType,
	(select IIF(unit.homeProvince=unit.currentLocation,unitType.baseMaintenanceCost/2,unitType.baseMaintenanceCost)) as unitMaintenanceInTwelfts
	from armyUnit unit, landUnitType unitType
	where unitType.id = unit.type;
go

-- luodaan armeijan ylläpitoyhteenvetonäkymä
CREATE VIEW armyDomainSumView AS
	select d.id as domainId, ISNULL(sum(unit.size),0) as armyManpower, ISNULL(sum(unit.unitMaintenanceInTwelfts),0) as unitMaintenanceInTwelfths
	from domain d left outer join armyUnitView unit on unit.ownerDomain = d.id
	group by d.id;
go

--- TODO jatka uuden birthright systeemin mukaisilla domain costien laskemisilla
--- täältä löytyy komponentteja, mitä ne maksaa... 
--- http://www.birthright.net/forums/showwiki.php?title=BRCS:Chapter_five_Ruling_a_domain_Components_of_a_domain&redirect=no 
--- täältä lisää infoa, kun väännät uudemmaksi http://www.birthright.net/forums/showwiki.php?title=BRCS:Chapter_five_Ruling_a_domain_Domain_collections
CREATE VIEW domainSumView AS
 select d.id as domainId,d.name as valtakunta, regent.name as hallitsija, 
	maakunta.population as population, armySum.armyManpower as armyManpower,
	(maakunta.sumProvinceLvl + holding.sumHoldingsLvl) as potentialRegency,
	cast((10*(maakunta.incomeGB*12+holding.incomeInTwelfts - d.courtLvl*12 - maakunta.maintenanceInTwelfts - armySum.unitMaintenanceInTwelfths)/12)/10.0 as numeric(16,1)) as incomeGB,
	cast((10*maakunta.incomeGB+(10*holding.incomeInTwelfts/12))/10.0 as numeric(16,1)) as totalIncomeGB,
	d.courtLvl as courtGB,
	cast(((10*maakunta.maintenanceInTwelfts/12))/10.0 as numeric(16,1)) as provinceMaintenanceGB,
	--maakunta.sumProvinceTaxationAverage as sumAvgTaxation, holding.sumAvgCollection as sumAvgCollection,
	((maakunta.incomeGB*12)+holding.incomeInTwelfts) as totalIncomeInTwelfts,
	(maakunta.maintenanceInTwelfts) as provinceMaintenanceInTwelfts,
	armySum.unitMaintenanceInTwelfths as armyMaintenanceInTwelfts,
	maakunta.lkmProvinces as lkmProvinces, maakunta.sumProvinceLvl as sumProvinceLvl, holding.lkmHoldings as lkmHoldings, holding.sumHoldingsLvl as sumHoldingsLvl
	
 from domain d,maakuntaDomainSumView maakunta, holdingDomainCollectionsSumView holding, person regent, armyDomainSumView armySum
 where maakunta.domainId=d.id and holding.domainId = d.id and regent.id = d.regent and armySum.domainId = d.id;
go

insert into loyalty (id,description) values 
 (0,'rebellious'),
 (1,'poor'),
 (2,'average'),
 (3,'high');
go

insert into taxes (id,description) values
	(0,'no taxes'),
	(1,'light taxes'),
	(2,'moderate taxes'),
	(3,'severe taxes');
go

insert into provinceLvl (id,description, population) values 
	(0,'unsettled wilderness',0),
	(1,'rural',1000),
	(2,'rural',4000),
	(3,'rural',7000),
	(4,'settled',10000),
	(5,'settled',20000),
	(6,'settled',30000),
	(7,'densely settled',40000),
	(8,'densely settled',60000),
	(9,'densely settled',80000),
	(10,'urban',100000);

insert into terrain (name,maxProvinceLvl,basePotential,moveCost) values 
	('Desert',3,5,1),
	('Glacier',1,5,4),
	('Heavy Forest',6,8,2),
	('Ancient Forest',6,9,2),
	('Hills',9,5,2),
	('Light Forest',8,7,1),
	('Marsh',6,5,2),
	('Swamp',6,8,2),
	('Low Mountains',7,7,2),
	('Medium Mountains',5,8,3),
	('High Mountains',3,9,4),
	('Moor',6,5,2),
	('Highland',6,6,2),
	('Plains',10,5,1),
	('Steppes',6,5,1),
	('Tundra',2,5,2);
go

/*

insert into provinceTaxation (provinceLvl,taxRate,taxation,average) values 
	(0,0,'-',0),	(0,1,'-',0),		(0,2,'-',0),		(0,3,'d3-2',0.3),
	(1,0,'-',0),	(1,1,'d3-2',0.3),	(1,2,'d3-1',1),		(1,3,'d3',2),
	(2,0,'-',0),	(2,1,'d3-1',1),		(2,2,'d3',2),		(2,3,'d4',2.5),
	(3,0,'-',0),	(3,1,'d3',2),		(3,2,'d4',2.5),		(3,3,'d4+1',3.5),
	(4,0,'-',0),	(4,1,'d4',2.5),		(4,2,'d4+1',3.5),	(4,3,'d6+1',4.5),
	(5,0,'-',0),	(5,1,'d4+1',3.5),	(5,2,'d6+1',4.5),	(5,3,'d8+1',5.5),
	(6,0,'-',0),	(6,1,'d6+1',4.5),	(6,2,'d8+1',5.5),	(6,3,'d10+1',6.5),
	(7,0,'-',0),	(7,1,'d8+1',5.5),	(7,2,'d10+1',6.5),	(7,3,'d12+1',7.5),
	(8,0,'-',0),	(8,1,'d10+1',6.5),	(8,2,'d12+1',7.5),	(8,3,'2d8',9),
	(9,0,'-',0),	(9,1,'d12+1',7.5),	(9,2,'2d8',9),		(9,3,'2d8+2',11),
	(10,0,'-',0),	(10,1,'2d8',9),		(10,2,'2d8+2',11),	(10,3,'2d10+2',13);
go



insert into guildAndTempleCollection(provinceLvl, holdingLvl, collection, average) values
	(0,1,'-',0),		(0,2,'-',0),		(0,3,'-',0),		(0,4,'-',0),		(0,5,'-',0),		(0,6,'-',0),		(0,7,'-',0),		(0,8,'-',0),		(0,9,'-',0),		(0,10,'-',0), 
	(1,1,'d3-2',0.3),	(1,2,'-',0),		(1,3,'-',0),		(1,4,'-',0),		(1,5,'-',0),		(1,6,'-',0),		(1,7,'-',0),		(1,8,'-',0),		(1,9,'-',0),		(1,10,'-',0), 
	(2,1,'d2-1',0.5),	(2,2,'d2',1.5),		(2,3,'-',0),		(2,4,'-',0),		(2,5,'-',0),		(2,6,'-',0),		(2,7,'-',0),		(2,8,'-',0),		(2,9,'-',0),		(2,10,'-',0), 
	(3,1,'d2-1',0.5),	(3,2,'d3',2),		(3,3,'d4',2.5),		(3,4,'-',0),		(3,5,'-',0),		(3,6,'-',0),		(3,7,'-',0),		(3,8,'-',0),		(3,9,'-',0),		(3,10,'-',0), 
	(4,1,'d2',1.5),		(4,2,'d3',2),		(4,3,'d4',2.5),		(4,4,'d6',3.5),		(4,5,'-',0),		(4,6,'-',0),		(4,7,'-',0),		(4,8,'-',0),		(4,9,'-',0),		(4,10,'-',0), 
	(5,1,'d2',1.5),		(5,2,'d3',2),		(5,3,'d4',2.5),		(5,4,'d6',3.5),		(5,5,'d6',3.5),		(5,6,'-',0),		(5,7,'-',0),		(5,8,'-',0),		(5,9,'-',0),		(5,10,'-',0), 
	(6,1,'d2',1.5),		(6,2,'d3',2),		(6,3,'2d2',3),		(6,4,'2d3',4),		(6,5,'2d3',4),		(6,6,'2d4+1',6),	(6,7,'-',0),		(6,8,'-',0),		(6,9,'-',0),		(6,10,'-',0), 
	(7,1,'d3',2),		(7,2,'d2+1',2.5),	(7,3,'d4+1',3.5),	(7,4,'d6+1',4.5),	(7,5,'d6+1',4.5),	(7,6,'2d4+2',7),	(7,7,'2d4+2',7),	(7,8,'-',0),		(7,9,'-',0),		(7,10,'-',0), 
	(8,1,'d3',2),		(8,2,'d2+1',2.5),	(8,3,'d4+1',3.5),	(8,4,'d6+1',4.5),	(8,5,'d6+1',4.5),	(8,6,'2d4+2',7),	(8,7,'2d4+2',7),	(8,8,'2d4+2',7),	(8,9,'-',0),		(8,10,'-',0), 
	(9,1,'d3',2),		(9,2,'d2+1',2.5),	(9,3,'d4+1',3.5),	(9,4,'d6+1',4.5),	(9,5,'d6+1',4.5),	(9,6,'2d4+2',7),	(9,7,'2d4+2',7),	(9,8,'2d4+2',7),	(9,9,'2d4+2',7),	(9,10,'-',0), 
	(10,1,'d3',2),		(10,2,'d2+1',2.5),	(10,3,'d4+1',3.5),	(10,4,'d6+1',4.5),	(10,5,'d6+1',4.5),	(10,6,'2d4+2',7),	(10,7,'2d4+2',7),	(10,8,'2d4+2',7),	(10,9,'2d4+2',7),	(10,10,'2d4+2',7);
go
*/
insert into holdingsType(name,generateCollection,incomeLvl) values 
	('Guild',1,8),
	('Law',0,4),
	('Temple',1,8),
	('Sources',0,0);
go

insert into landUnitType(name,baseMusterCost,baseMaintenanceCost,draft,mercenary) values
	('Archers',24,12,0,0),
	('Longbowmen',36,18,0,0),
	('Light Footmen',12,12,0,0),
	('Heavy Footmen',24,12,0,0),
	('Pikemen',36,12,0,0),
	('Light Cavalry',36,24,0,0),
	('Medium Cavalry',48,24,0,0),
	('Knights',72,36,0,0);
	