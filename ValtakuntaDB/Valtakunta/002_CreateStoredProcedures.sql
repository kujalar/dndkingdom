use DnDValtakunta;
go


---todo maxLevel (+2 coast(done), +1 major river if not coast and has major river (not done yet)...)
CREATE PROCEDURE createProvince @domainName varchar(255),@provinceName varchar(255), @terrainName varchar(24), 
	@coastValue integer, @level integer, @fortLevel integer, @loyalityValue integer, @taxRate integer AS
	
	DECLARE @noSeaport int = 0;
	DECLARE @noHighway int = 0;
	
	insert into province(name,domain,terrain,loyalty,coast,lvl,fortLvl,maxSource,taxrate,maxLvl,seaport,highway,woodBridges,stoneBridges,ferries) 
		select @provinceName,domain.id ,terrain.id,@loyalityValue,@coastValue
			,@level,(select @fortLevel),(select IIF (@coastValue>0 and terrain.basePotential<7 ,7,terrain.basePotential)),@taxRate,
			-- jos ollaan rannalla niin maaston MaxProvinceLevel on +2 ja jos ei, niin se on maxProvinceLvl (10 on ehdoton maksimi)
			(select IIF(@coastValue>0 , (select IIF(terrain.maxProvinceLvl<8,terrain.maxProvinceLvl+2,10)),terrain.maxProvinceLvl)),
			(select @noSeaport),(select @noHighway), 
			-- siltaarvojen defaultit 0
			0,0,0
		from terrain, domain
		where terrain.name = (select @terrainName) and domain.name = (select @domainName);
go

CREATE PROCEDURE addSeaport @provinceName varchar(255) AS
	update province set seaport = 1
	where province.name = (select @provinceName);
go

CREATE PROCEDURE addHighway @provinceName varchar(255) AS
	update province set highway = 1
	where province.name = (select @provinceName);
go

CREATE PROCEDURE setBridges @provinceName varchar(255), @woodBridges int, @stoneBridges int, @ferries int AS
	update province set woodBridges = @woodBridges, stoneBridges = @stoneBridges, ferries = @ferries
	where province.name = (select @provinceName);
go
--- tarvitaan seuraavaksi createHoldings procedure

---declare @holdingsType nvarchar(16);
---declare @locationName varchar(255);
---declare @domainName varchar(255);
---declare @holdingsLvl integer;
---set @holdingsType = 'Guild';
---set @locationName = 'Hernari';
---set @domainName = 'Hernemaa';
---set @holdingsLvl = 1;


CREATE PROCEDURE createHolding @holdingsName varchar(255), @holdingsType nvarchar(16), @domainName varchar(255), @locationName varchar(255), @holdingsLvl integer AS
	insert into holdings(name,type,location,ownerDomain,lvl,fortLvl)
	select @holdingsName,holdingsType.id,province.id,domain.id,@holdingsLvl,0
		from holdingsType,province,domain
		where holdingsType.name = (select @holdingsType) and province.name = (select @locationName)
		and domain.name = (select @domainName);
go

CREATE PROCEDURE holdings_setFortLvl @holdingsName varchar(255), @fortLvl int AS
	update holdings set fortLvl = @fortLvl 
	where name = (select @holdingsName);
go

--- http://stackoverflow.com/questions/42648/best-way-to-get-identity-of-inserted-row
--- nyt luodaan 2 guildin v�lill� oleva trade route guildA <-> guildB
CREATE PROCEDURE createTradeRoute @guildA varchar(255), @guildB varchar(255), @routeName varchar(255) AS
	declare @guildAId integer;
	declare @guildBId integer;
	declare @tradeRouteId integer;

	set @guildAId = (select h.id from holdings h where h.name = (select @guildA));
	set @guildBId = (select h.id from holdings h where h.name = (select @guildB));

	insert into tradeRoute(name) values (@routeName);
	set @tradeRouteId = SCOPE_IDENTITY();

	insert into guildTradeRoute(tradeRouteId,guildId)
		values	(@tradeRouteId,@guildAId),
				(@tradeRouteId,@guildBId);

go

CREATE PROCEDURE createArmyUnit @unitName varchar(255), @ownerDomain varchar(255), 
		@unitType varchar(255), @locationProvince varchar(255) AS
	
	insert into armyUnit(name,size,type,mercenary,currentLocation,homeProvince,ownerDomain) 
		select
		(select @unitName), 200,
		lut.id as landUnitType,
		lut.mercenary as mercenary,
		location.id as currentLocation, location.id as homeProvince,
		(select od.id from domain od where name = (select @ownerDomain)) as ownerDomain	
		from landUnitType lut, province location
		where lut.name = (select @unitType)
		and location.name = (select @locationProvince);
go
