drop table holdings;
go
drop table province;
go
drop table domain;
go
drop table terrain;
go
drop table person;
go
drop table holdingsType;
go

create table person (
	id integer identity,
	name text,
	primary key (id)
);
go

create table holdingsType (
	id integer identity,
	name text,
	primary key (id)
);
go

create table terrain (
	id integer identity,
	name text,
	maxProvinceLvl integer,
	moveCost integer,
	primary key (id),
);
go

create table domain (
	id integer identity,
	regent integer,
	name text,
	primary key (id),
	CONSTRAINT fk_domain_person FOREIGN KEY (regent) REFERENCES person(id)
);
go

create table province (
    id integer identity,
	domain integer,
	terrain integer,
	name text,
	lvl integer,
	primary key (id),
	CONSTRAINT fk_province_domain FOREIGN KEY (domain) REFERENCES domain(id),
	CONSTRAINT fk_province_terrain FOREIGN KEY (terrain) REFERENCES terrain(id)
);
go

create table holdings (
	id integer identity,
	type integer,
	location integer,
	ownerDomain integer,
	lvl integer,
	primary key (id),
	CONSTRAINT fk_holdings_holdingstype FOREIGN KEY (type) REFERENCES holdingsType(id),
	CONSTRAINT fk_holdings_province FOREIGN KEY (location) REFERENCES province(id),
	CONSTRAINT fk_holdings_domain FOREIGN KEY (ownerDomain) REFERENCES domain(id)
);

insert into terrain (name,maxProvinceLvl,moveCost) values 
	('Desert',3,1),
	('Glacier',1,4),
	('Heavy Forest',6,2),
	('Hills',9,2),
	('Light Forest',8,1),
	('Marsh/Swamp',6,2),
	('Low Mountains',7,2),
	('Medium Mountains',5,3),
	('High Mountains',3,4),
	('Moor/Highland',6,2),
	('Plains',10,1),
	('Steppes',6,1),
	('Tundra',2,2);
go
